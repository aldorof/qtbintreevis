#include <QDebug>
#include <QSlider>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QLabel>
#include <QPainter>

#include "TreeNodeWidget.h"

//-------------------------------------------------------------------//
TreeNodeWidget::TreeNodeWidget(QWidget *parent, size_t index, TreeNode::NodeType type): 
        QWidget(parent), fIndex(index), fType(type) {
    qDebug() << "...TreeNodeWidget created at " << this;

    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->setContentsMargins(cLeftRectWidth + 2, 2, 2, 2);

    // Label will be on the top
    fLabel = new QLabel();
    QColor label_color(180, 180, 180);
    QPalette palette = fLabel->palette();
    palette.setColor(fLabel->foregroundRole(), label_color);
    fLabel->setPalette(palette);
    layout->addWidget(fLabel);

    // Vertical strech to separate Title from Value
    layout->addStretch();
    switch ( fType ) {
        case TreeNode::NodeType::String:
            fLineEdit = new QLineEdit();
            connect( fLineEdit, SIGNAL( editingFinished() ),
                     this,        SLOT( LineEditValueChanged() ) );
            layout->addWidget(fLineEdit);
            break;
        case TreeNode::NodeType::Float:
            fSlider = new QSlider(Qt::Horizontal);
            fSlider->setMinimum(-100 );
            fSlider->setMaximum( 100 );
            connect( fSlider,  SIGNAL( valueChanged(int) ),
                     this,       SLOT( SliderValueChanged(int) ) );
            layout->addWidget(fSlider);
            break;
    }
}
//-------------------------------------------------------------------//
TreeNodeWidget::~TreeNodeWidget(){
    qDebug() << "...TreeNodeWidget destructed at " << this;
}
//-------------------------------------------------------------------//
void TreeNodeWidget::SetTitle(QString title) {
    fLabel->setText(title);
}
//-------------------------------------------------------------------//
void TreeNodeWidget::SetValue(QVariant value) {
    switch ( fType ) {
        case TreeNode::NodeType::String:
            fLineEdit->setText(value.toString());
            break;
        case TreeNode::NodeType::Float:
            fSlider->setValue( value.toFloat() );
            break;
    }
}
//-------------------------------------------------------------------//
void TreeNodeWidget::paintEvent(QPaintEvent *event){
    QColor left_rect_color(0, 99, 116);
    QColor right_rect_color(54, 54, 54);

    QRect left_rect(0, 0, cLeftRectWidth, height() );
    QRect right_rect(cLeftRectWidth, 0, width(), height() );

    QPainter painter(this);
    painter.fillRect(left_rect, left_rect_color);
    painter.fillRect(right_rect, right_rect_color);

    QWidget::paintEvent(event);
}
//-------------------------------------------------------------------//
void TreeNodeWidget::SliderValueChanged(int value) {
    emit ValueChanged( fIndex, QVariant((float)value) );
}
//-------------------------------------------------------------------//
void TreeNodeWidget::LineEditValueChanged() {
    emit ValueChanged( fIndex, QVariant(fLineEdit->text()) );
}
//-------------------------------------------------------------------//
