#ifndef TREENODE_H_
#define TREENODE_H_

#include <string>
#include <variant>

/**
 * This class holds the data associated with the node.
 */
class TreeNode {
    friend class TreeModel;
    friend class TreeView;
  public: 
    enum NodeType {
        String,
        Float
    };
    /**
     * Constructor
     */
    TreeNode();
    /**
     * Destructor
     */
    ~TreeNode();
    /**
     * Helper Function to return the type of the node
     */
    NodeType GetType();
  public:
    std::string fTitle;
    std::variant <float, std::string> fValue;
  private:
    std::vector <size_t> fParents;
    std::vector <size_t> fChildren;
};

#endif /* TREENODE_H_ */
