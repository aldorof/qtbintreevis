#include <QApplication>

#include <sstream> // for std::ostringstream

#include "TreeModel.h"
#include "TreeView.h"

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);

    TreeModel model(NULL);
    // Let's add some simple data by default
    // This is DAG tree (Node 6 has 2 parents)
    // with 11 nodes.
    // It has 4 nodes with 0 children
    //        3 nodes with 1 child
    //        4 nodes with 2 children
    // The node type is set to either String or Float
    //
    //   0
    //   /\
    //  1  2
    //  |  /\
    //  3 4  5
    //   \/\
    //   6  7
    //   /\  |
    //  8  9 10
    model.SetRoot(0);
    for (auto ii = 0; ii <= 10; ++ii) {
        model.AddNode( ii, (ii % 3 == 0) ? 
            TreeNode::NodeType::String : TreeNode::NodeType::Float );

        // Let's place unique title
        std::ostringstream node_title;
        node_title << "Title " << ii;
        model.GetNode(ii).fTitle = node_title.str();

        // And unique values [-100..100]
        std::ostringstream value_string;
        switch (model.GetNode(ii).GetType()) {
            case TreeNode::NodeType::String :
                value_string << ii*10 << ".0";
                model.GetNode(ii).fValue = value_string.str();
                break;
            case TreeNode::NodeType::Float :
                model.GetNode(ii).fValue = ii*10.0f ;
                break;
        }
    }
    model.AddEdge(0, 1);
    model.AddEdge(0, 2);
    model.AddEdge(1, 3);
    model.AddEdge(2, 4);
    model.AddEdge(2, 5);
    model.AddEdge(3, 6);
    model.AddEdge(4, 6);
    model.AddEdge(4, 7);
    model.AddEdge(6, 8);
    model.AddEdge(6, 9);
    model.AddEdge(7, 10);

    TreeView view(NULL);
    view.SetModel(&model);
    view.setWindowTitle("Qt Widget Tree Visualizer");
    view.resize(600, 800);
    view.show();

    return app.exec();
}
