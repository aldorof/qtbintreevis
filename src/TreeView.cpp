#include <QDebug>
#include <QPainter>

#include "TreeView.h"
#include "TreeModel.h"
#include "TreeNodeWidget.h"

//-------------------------------------------------------------------//
TreeView::TreeView(QWidget *parent): QWidget(parent) {
    qDebug() << "...TreeView created at " << this;    
}
//-------------------------------------------------------------------//
TreeView::~TreeView(){
    qDebug() << "...TreeView destructed at " << this;
}
//-------------------------------------------------------------------//
void TreeView::paintEvent(QPaintEvent *event) {
    QColor edge_line_color(185, 109, 19);
    QPen edge_pen(edge_line_color);
    edge_pen.setWidth(cEdgeWidth);

    QPainter painter(this);
    painter.setPen(edge_pen);

    // Draw edges from the center of the parent to the center of the child
    for (auto it=fEdges.cbegin(); it != fEdges.cend(); ++it) {
        auto parent_it = fNodes.find(it->first);
        auto child_it = fNodes.find(it->second);
        if ( (parent_it == fNodes.end()) || (child_it == fNodes.end()) ) {
            qWarning() << "...TreeView: wrong data in fEdges";
            continue;
        }
        TreeNodeWidget *parent = parent_it->second;
        TreeNodeWidget *child = child_it->second;
        if ( (parent == NULL)||(child == NULL) ) {
            qWarning() << "...TreeView: wrong data in fNodes";
            continue;
        }
        painter.drawLine( parent->x() + (cNodeWidth / 2), parent->y() + (cNodeHeight / 2),
                          child->x() + (cNodeWidth / 2),  child->y() + (cNodeHeight / 2) );
    }
    QWidget::paintEvent(event);
}
//-------------------------------------------------------------------//
void TreeView::SetModel(TreeModel *model) {
    // Clear edges before the nodes
    fEdges.clear();
    // Let's clear the old nodes first if any exist
    for (auto it = fNodes.cbegin(); it != fNodes.cend(); ++it) {
        delete (it->second);
    }
    fNodes.clear();
    // We do need to disconnect from old model if there are any connections
    disconnect(this,  SIGNAL( NodeValueChanged(size_t, QVariant) ),  NULL, NULL);
    // If model pnt is null just exit at this point
    if (model == NULL) {
        return;
    }

    std::vector <size_t> parent_row;
    std::vector <size_t> children_row;
    // Start with root node
    parent_row.push_back(model->GetRoot());
    int node_row = 0;
    TreeNodeWidget *test_widget;
    while ( !parent_row.empty() ) {
        int node_column = 0;
        // Let's go through parents and retrieve all children
        for (auto parent_it = parent_row.cbegin(); parent_it != parent_row.cend(); ++parent_it) {
            TreeNode &parent_node = model->GetNode(*parent_it);
            test_widget = AddNode(*parent_it, parent_node.GetType() );
            if ( test_widget != NULL ) {
                test_widget->SetTitle( QString::fromStdString(parent_node.fTitle) );
                switch ( parent_node.GetType() ) {
                    case TreeNode::NodeType::String:
                        test_widget->SetValue(QVariant(QString::fromStdString(std::get<std::string>(parent_node.fValue))));
                        break;
                    case TreeNode::NodeType::Float:
                        test_widget->SetValue(QVariant(std::get<float>(parent_node.fValue)));
                        break;
                }

                test_widget->move( node_column*(cNodeWidth+20), node_row*(cNodeHeight+20) );
                node_column ++;
            }
            for (auto child_it = parent_node.fChildren.cbegin(); child_it != parent_node.fChildren.cend(); ++child_it) {
                children_row.push_back(*child_it);
                fEdges.push_back({*parent_it, *child_it});
            }
        }
        node_row ++;
        parent_row = children_row;
        children_row.clear();
    }
    // Last, but not least, connect Node values feedback signal to the model
    connect( this,    SIGNAL( NodeValueChanged(size_t, QVariant) ),
             model,     SLOT( NodeValueChanged(size_t, QVariant) ) );
    
}
//-------------------------------------------------------------------//
TreeNodeWidget* TreeView::AddNode( size_t index, TreeNode::NodeType type ){
    TreeNodeWidget* result = NULL;
    if ( fNodes.find(index) != fNodes.end() ) {
        return result;
    }
    result = new TreeNodeWidget( this, index, type );
    if (result == NULL) {
        // Out of mem protection
        return result;
    }
    connect( result,    SIGNAL( ValueChanged(size_t, QVariant) ),
             this,      SIGNAL( NodeValueChanged(size_t, QVariant) ) );

    fNodes.emplace( index, result );
    result->resize( cNodeWidth, cNodeHeight );

    return result;
}
//-------------------------------------------------------------------//
