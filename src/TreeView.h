#ifndef TREEVIEW_H_
#define TREEVIEW_H_

#include <QWidget>
#include <QVariant>

#include "TreeNode.h"

// forward declarations
class TreeModel;
class TreeNodeWidget;
/**
 * This class is top hierarchy widget for showing the tree.
 */
class TreeView : public QWidget {
    Q_OBJECT
  public:
    /**
     * Constructor
     */
    TreeView(QWidget *parent);
    /**
     * Destructor
     */
    ~TreeView();
    /**
     * Reads the model tree, creates and places GUI widgets
     * corresponding to nodes into this widget and connects signals/slots
     * to the correponding model.
     */
    void SetModel(TreeModel *model);
  signals:
    /**
     * This is combined signal for all node widgets indicating that the node
     * value was changed.
     */
    void NodeValueChanged(size_t index, QVariant value);
  protected:
    /**
     * Overriden repaint function to draw node edges.
     */
    void paintEvent(QPaintEvent *event) override;
  private:
    /**
     * Adds Node Widget if the index is not already in the map, returns
     * the pointer of created node widget. WARNING: can return NULL.
     */
    TreeNodeWidget* AddNode(size_t index, TreeNode::NodeType type);
  private:
    static constexpr int cNodeWidth = 100;
    static constexpr int cNodeHeight = 80;
    static constexpr int cEdgeWidth = 5;

    std::unordered_map<size_t, TreeNodeWidget*> fNodes; // Stored nodes widgets
    std::vector<std::pair<size_t, size_t>> fEdges; // Stores the nodes connection map
};

#endif /* TREEVIEW_H_ */
