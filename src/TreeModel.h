#ifndef TREEMODEL_H_
#define TREEMODEL_H_

#include <QObject>
#include <QVariant>

#include "TreeNode.h"
/**
 * This class would hold a TreeNode collection.
 */
class TreeModel : public QObject {
    Q_OBJECT
  public:
    /**
     * Constructor
     */
    TreeModel(QObject *parent);
    /**
     * Destructor
     */
    ~TreeModel();
    /**
     * Sets the root to node with the given index
     * The node should exist
     */
    void SetRoot(size_t node_index);
    /**
     * Returns the index of root node
     */
    size_t GetRoot();
    /**
     * Adds a Node to the tree
     */
    void AddNode(size_t node_index, TreeNode::NodeType type);
    /**
     * Connects the edge from parent to child node
     * The Nodes should be existing
     */
    void AddEdge(size_t parent, size_t child);
    /**
     * Returns a Node delegate with a given index
     */
    TreeNode &GetNode(size_t index);
  public slots:
    /**
     * This is feedback slot from TreeView that indicates node value change by user.
     */
    void NodeValueChanged(size_t index, QVariant value);

  private:
    std::unordered_map <size_t, TreeNode> fNodes;
    size_t fRootIndex; // Index of the root node
};

#endif /* TREEMODEL_H_ */
