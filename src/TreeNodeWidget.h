#ifndef TREENODEWIDGET_H_
#define TREENODEWIDGET_H_

#include <QWidget>
#include <QVariant>
class QLabel;
class QSlider;
class QLineEdit;

#include "TreeNode.h"
/**
 * This class is for storing and rendering tree node.
 */
class TreeNodeWidget : public QWidget {
    Q_OBJECT
  public: 
    /**
     * Constructor
     */
    TreeNodeWidget(QWidget *parent, size_t index, TreeNode::NodeType type);
    /**
     * Destructor
     */
    ~TreeNodeWidget();
    /**
     * Sets the Title
     */
    void SetTitle(QString title);
    /**
     * Sets the Value
     */
    void SetValue(QVariant value);
  signals:
    /**
     * This signal will be emitted every time there is a node value change
     */
    void ValueChanged(size_t index, QVariant value);
  protected:
    /**
     * Override of repainting function, to insert custom 2-rectangle style
     */
    void paintEvent(QPaintEvent *event) override;
  private slots:
    /**
     * This slot is for changes propagation from fSlider back to the model value.
     */
    void SliderValueChanged(int value);
    /**
     * This slot is for changes propagation from fLineEdit back to the model value.
     */
    void LineEditValueChanged();
  private:
    static constexpr int cLeftRectWidth = 10; // Width of the left rectangle

    size_t fIndex; // This would hold node index to report the changes back to the model
    TreeNode::NodeType fType; // Node type

    QLabel *fLabel;
    QSlider *fSlider;
    QLineEdit *fLineEdit;
};

#endif /* TREENODEWIDGET_H_ */
