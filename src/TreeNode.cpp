#include <QDebug>

#include "TreeNode.h"

//-------------------------------------------------------------------//
TreeNode::TreeNode() {
    qDebug() << "...TreeNode created at " << this;    
}
//-------------------------------------------------------------------//
TreeNode::~TreeNode(){
    qDebug() << "...TreeNode destructed at " << this;
}
//-------------------------------------------------------------------//
TreeNode::NodeType TreeNode::GetType() {
    if ( std::holds_alternative<float>(fValue) ) 
        return NodeType::Float;
    else
        return NodeType::String;
}
//-------------------------------------------------------------------//
