#include <QDebug>

#include <iostream> // for std::cout

#include "TreeModel.h"

//-------------------------------------------------------------------//
TreeModel::TreeModel(QObject *parent) :
        QObject(parent), fRootIndex(0){
    qDebug() << "...TreeModel created at " << this;    
}
//-------------------------------------------------------------------//
TreeModel::~TreeModel(){
    qDebug() << "...TreeModel destructed at " << this;
}
//-------------------------------------------------------------------//
void TreeModel::SetRoot(size_t node_index){
    fRootIndex = node_index;
}
//-------------------------------------------------------------------//
size_t TreeModel::GetRoot() {
    return fRootIndex;
}
//-------------------------------------------------------------------//
void TreeModel::AddNode(size_t node_index, TreeNode::NodeType type){
    switch (type) {
        case TreeNode::NodeType::String:
            fNodes[node_index].fValue = "0.0";
            break;
        case TreeNode::NodeType::Float:
            fNodes[node_index].fValue = 0.0f;
            break;
    }
}
//-------------------------------------------------------------------//
void TreeModel::AddEdge(size_t parent, size_t child){
    // For now let's not do any checks and just create double-linked list
    fNodes[parent].fChildren.push_back(child);
    fNodes[child].fParents.push_back(parent);
}
//-------------------------------------------------------------------//
TreeNode &TreeModel::GetNode(size_t index){
    return fNodes[index];
}
//-------------------------------------------------------------------//
void TreeModel::NodeValueChanged(size_t index, QVariant value){
    // qDebug() << "...TreeModel: node " << index << " value " << value << " changed";
    if ( strcmp(value.typeName(), "QString") == 0 ) {
        fNodes[index].fValue = value.toString().toStdString();
        std::cout << "...TreeModel: node " << index << " value \"" << std::get<std::string>(fNodes[index].fValue) << "\"" << std::endl;
    } else if ( strcmp(value.typeName(), "float") == 0 ) {
        fNodes[index].fValue = value.toFloat();
        std::cout << "...TreeModel: node " << index << " value " << std::get<float>(fNodes[index].fValue) << std::endl;
    } else {
        qWarning() << "...TreeModel: Unknown value type " << value.typeName();
    }
}
//-------------------------------------------------------------------//
