Qt Widget Binary Tree Visualizer Demo
=====================================

Preface
-------
This demo was created as a part of coding exercise.
It shows the simplest way of how to visualize binary (but it's not limited to only binary)
tree of nodes using Qt Widgets. There are much more beautiful ways 
of [visualizing the trees](https://treevis.net) elsewhere.

Specs
-----
There were the following requirements/specs given (in arbitrary order) which are present
in the final realization:

* Visualization should use Qt Widgets only (no QML).
* The tree structure is fixed (there is no nodes removal or addition in the model).
* Example tree should have more than 10 nodes (11 nodes were used in the example) and
there should be nodes with 0, 1 and 2 children in the tree.
* Each node has two fields associated with it: ```title``` and ```value```.
    * ```title``` has string type and can't be changed by user
    * ```value``` has a variant type, depending on the node type and can be changed by user.
* Each node can be of two types: ```Float``` or ```String```:
    * For ```Float``` node the ```value``` is of float type which is adjusted with slider.
    * For ```String``` node the ```value``` is of string type which is adjusted with text field.
* Each node shold have custom background.
* Nodes should be connected according to the tree structure.
* Modern C++ > 14 should be used (```std::variant``` was used for the node ```value``` which is C++17).
* Visualization of [DAG tree](https://en.wikipedia.org/wiki/Directed_acyclic_graph) (Node 6 in the demo has 2 parents).
* Tree structure for the demo can be changed in ```main.cpp```.

Brief Structure Description
---------------------------
The demo vaguely follows Qt scheme of [model/view](https://doc.qt.io/qt-5/model-view-programming.html) with 
signal/slot mechanism of data delegation.

```TreeModel``` class holds map of ```TreeNode```s, each is uniquely identified with the index. 
Each ```TreeNode``` holds pure C++ description (no Qt classes) of node data fields as well as 
connection to other nodes.

```TreeView``` class holds map of ```TreeNodeWidget```s, each identified with the same index 
scheme as in ```TreeModel```.

```TreeView``` synchronizing it's state with ```TreeModel``` upon call to ```TreeView::SetModel``` function.
User changes are propagated back to ```TreeModel``` via ```TreeView::NodeValueChanged``` signal, connected
to ```TreeModel::NodeValueChanged``` slot.

Packages
--------
Windows 64bit [executable](https://gitlab.com/aldorof/qtbintreevis/-/package_files/23222756/download) 
bundled with Qt libraries can be found [here](https://gitlab.com/aldorof/qtbintreevis/-/packages/4056041).

Building
--------
[Instructions](BUILDING.md) for building this demo.

Dependencies
------------
* [Qt5](https://code.qt.io/cgit/qt/qt5.git) (GPLv3 License).

License
----------------
This demo is released under the terms of the [GPLv3](LICENSE.md).
