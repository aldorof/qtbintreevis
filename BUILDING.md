# Building QtBinTreeVis
Summary of building process for different platforms
## Table of contents
* [Building on Linux](#building-on-linux)
* [Building on Windows](#building-on-windows)

## Building on Linux
1. Install [Qt5 prerequisites](https://doc.qt.io/qt-5/linux-requirements.html).
2. Build Qt from git source.
    *  ``` git clone git://code.qt.io/qt/qt5.git ```
    * ``` cd qt5 ```
    * ``` git checkout 5.15.2 ```
    * ``` git submodule update --init --recursive ```
    * For gcc 11 the following patch should be applied to ``` qtbase/src/corelib/global/qglobal.h ``` :
    ```
        @@ -45,6 +45,7 @@
     #  include <type_traits>
     #  include <cstddef>
     #  include <utility>
    +#  include <limits>
     #endif
     #ifndef __ASSEMBLER__
     #  include <assert.h>
    ```
    * ``` mkdir ../qt5-build ```
    * ``` cd ../qt5-build ```
    * ```
        ../qt5/configure \
            -platform linux-g++ \
            -prefix ../install \
            -archdatadir ../install/lib \
            -datadir ../install/share \
            -release -opensource -confirm-license \
            -nomake examples -nomake tests -nomake tools \
            -no-cups -no-sql-db2 -no-sql-ibase -no-sql-mysql \
            -no-sql-oci -no-sql-odbc -no-sql-psql -no-sql-sqlite -no-sql-sqlite2 -no-sql-tds \
            -no-icu -no-harfbuzz -qt-libpng -no-openssl -qt-pcre -no-eglfs \
            -skip qtactiveqt \
            -skip qtcanvas3d \
            -skip qtcharts \
            -skip qtconnectivity \
            -skip qtdatavis3d \
            -skip qtdoc \
            -skip qtdocgallery \
            -skip qtfeedback \
            -skip qtgamepad \
            -skip qtlocation \
            -skip qtmultimedia \
            -skip qtpim \
            -skip qtpurchasing \
            -skip qtquick3d \
            -skip qtremoteobjects \
            -skip qtscript \
            -skip qtsensors \
            -skip qtspeech \
            -skip qttools \
            -skip qtvirtualkeyboard \
            -skip qtwayland \
            -skip qtwebchannel \
            -skip qtwebengine \
            -skip qtwebsockets \
            -skip qtwebview \
            -skip qtxmlpatterns \
            -skip qt3d \
            -skip qtmacextras -skip qtwinextras -skip qtandroidextras \
            -xcb -no-rpath
      ```
    * ``` make -j15 ```
    * ``` make install ```
    * ``` cd ../ ```
3. Build QtBinTreeVis executable
    * ``` git clone https://gitlab.com/aldorof/qtbintreevis.git```
    * ``` mkdir qtbintreevis-build ```
    * ``` cd qtbintreevis-build ```
    * ``` cmake -DCMAKE_INSTALL_PREFIX=../install ../qtbintreevis ```
    * ``` make -j15 ```
    * NOTE: To run executable: ``` LD_LIBRARY_PATH=$PWD/../install/lib ./bintree_vis```
## Building on Windows
1. Install prerequisites.
    * [Git for windows](https://gitforwindows.org/)
    * [CMake for windows](https://cmake.org/)
    * [Perl for windows](https://www.perl.org/get.html) required to build Qt5.
        * Install to ```C:\Perl64```
    * [Python for windows](https://www.python.org/downloads/windows/) required to build Qt5.
        * Install to ```C:\Python39```
    * [NSIS](http://nsis.sourceforge.net/Main_Page) needed to create the installer.
    * Mingw64 from [MSYS2](https://www.msys2.org/)
        * ```C:\msys64\usr\bin\bash -lc ' '```
        * ```C:\msys64\usr\bin\bash -lc 'pacman --noconfirm -Syuu' ```
        * (to run console) ```C:/msys64/msys2_shell.cmd -defterm -here -no-start -mingw64 ```
            * (in the console) ```pacman -S --needed base-devel mingw-w64-x86_64-toolchain ```
            * (in the console) Add ```export MSYS2_ARG_CONV_EXCL="--foreign-types="``` to ```.bash_profile```
        * Repeat for x32 ```C:/msys64/msys2_shell.cmd -defterm -here -no-start -mingw32```
            * (in the console) ```pacman -S --needed base-devel mingw-w64-i686-toolchain ```
        * (if the process get stuck) ```taskkill /F /FI "MODULES eq msys-2.0.dll"```
    * Microsoft [DirectX SDK](https://www.microsoft.com/en-us/download/details.aspx?id=6812)
2. Setup build environment (IMPORTANT):
    * Let's start with clean environment
    ```
    set PATH=C:\WINDOWS\system32;C:\WINDOWS;
    ```
    * There is an executable ```fxc.exe``` that Qt build uses when built with MinGW so the
    suggestion is to place it in your path if it's not there (for x64):
    ```
        set PATH=C:\Program Files (x86)\Windows Kits\10\bin\10.0.19041.0\x64;%PATH%
    ```
    or (for x86):
    ``` 
        set PATH=C:\Program Files (x86)\Windows Kits\10\bin\10.0.19041.0\x86;%PATH%
    ```
    test it with ```fxc /?```
    * Make sure the right MinGW itself is in your path (for x64):
    ```
    set PATH=%PATH%C:\msys64\mingw64\bin;
    ```
    or (for x86):
    ```
    set PATH=%PATH%C:\msys64\mingw32\bin;
    ```
    test it with ```gcc -v```
    * We should use ActivePerl and Python installed above , we might use CMake and Git later on, so we'll add it here too.
    ```
    set PATH=C:\Perl64\bin;C:\Python39;%PATH%C:\Program Files\CMake\bin;C:\Program Files\Git\cmd;
    ```
    test it with
    ```
    perl -v
    python -V
    cmake -h
    git --version
    ```
3. Build Qt from git source ( x64 used for example)
    * NOTE: Unfortunately it seems that there is only in-source build available for WIN.
    * NOTE: GCC has issues with long file paths for include directives on Windows. It may fail to find certain include files 
    if the absolute file path is too long. The best solution is to set up the project source folder so that it is near the 
    top of the drive with short folder names. An alias directory may also work, but hasn't been tested. Changing the 
    Windows settings to allow longer file paths will **not** be sufficient to fix the problem.
    * (from your home dir) ``` mkdir src ```
    * ``` cd src ```
    * ``` set _INSTALL_PATH=%cd%\install-mingw-x64 ```
    * ``` git clone git://code.qt.io/qt/qt5.git qt5-mingw-x64 ```
    * ``` cd qt5-mingw-x64 ```
    * ``` git checkout 5.15.2 ```
    * ``` git submodule update --init --recursive ```
    * Apply the following patch to ```qtdeclarative/src/plugins/scenegraph/d3d12/qsgd3d12engine.cpp```
    ```
        @@ -63,6 +63,8 @@

         #include <comdef.h>

        +#include <d3d12sdklayers.h>
        +
         QT_BEGIN_NAMESPACE

         // NOTE: Avoid categorized logging. It is slow.
        @@ -221,7 +223,7 @@ static void getHardwareAdapter(IDXGIFactory1 *factory, IDXGIAdapter1 **outAdapte
                 if (SUCCEEDED(factory->EnumAdapters1(adapterIndex, &adapter))) {
                     adapter->GetDesc1(&desc);
                     const QString name = QString::fromUtf16((char16_t *) desc.Description);
        -            HRESULT hr = D3D12CreateDevice(adapter.Get(), fl, _uuidof(ID3D12Device), nullptr);
        +            HRESULT hr = D3D12CreateDevice(adapter.Get(), fl, __uuidof(ID3D12Device), nullptr);
                     if (SUCCEEDED(hr)) {
                         qCDebug(QSG_LOG_INFO_GENERAL, "Using requested adapter '%s'", qPrintable(name));
                         *outAdapter = adapter.Detach();
        @@ -238,7 +240,7 @@ static void getHardwareAdapter(IDXGIFactory1 *factory, IDXGIAdapter1 **outAdapte
                 if (desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE)
                     continue;

        -        if (SUCCEEDED(D3D12CreateDevice(adapter.Get(), fl, _uuidof(ID3D12Device), nullptr))) {
        +        if (SUCCEEDED(D3D12CreateDevice(adapter.Get(), fl, __uuidof(ID3D12Device), nullptr))) {
                     const QString name = QString::fromUtf16((char16_t *) desc.Description);
                     qCDebug(QSG_LOG_INFO_GENERAL, "Using adapter '%s'", qPrintable(name));
                     break;
    ```
    * (all should be one line) platforms can be ```win32-msvc``` or ```win32-g++```
    ```
    .\configure.bat -platform win32-g++ -prefix %_INSTALL_PATH% -archdatadir %_INSTALL_PATH%\lib -datadir %_INSTALL_PATH%\share -release -opensource -confirm-license -nomake examples -nomake tests -nomake tools -no-cups -no-sql-db2 -no-sql-ibase -no-sql-mysql -no-sql-oci -no-sql-odbc -no-sql-psql -no-sql-sqlite -no-sql-sqlite2 -no-sql-tds -no-icu -no-harfbuzz -qt-libpng -no-openssl -qt-pcre -no-eglfs -skip qtactiveqt -skip qtcanvas3d -skip qtcharts -skip qtconnectivity -skip qtdatavis3d -skip qtdoc -skip qtdocgallery -skip qtfeedback -skip qtgamepad -skip qtlocation -skip qtmultimedia -skip qtpim -skip qtpurchasing -skip qtquick3d -skip qtremoteobjects -skip qtscript -skip qtsensors -skip qtspeech -skip qttools -skip qtvirtualkeyboard -skip qtwayland -skip qtwebchannel -skip qtwebengine -skip qtwebsockets -skip qtwebview -skip qtxmlpatterns -skip qt3d -opengl desktop -skip qtx11extras -skip qtmacextras -skip qtandroidextras
    ```
    * ```mingw32-make -j3```
    * ```mingw32-make install```
    * ```cd ..```
4. Build QtBinTreeVis package (x64 used for example, use x86 for 32bit)
    * ``` git clone https://gitlab.com/aldorof/qtbintreevis.git ```
    * ``` mkdir qtbintreevis-build-mingw-x64 ```
    * ``` cd qtbintreevis-build-mingw-x64 ```
    * ``` cmake -G "MinGW Makefiles" -DCMAKE_INSTALL_PREFIX=..\install-mingw-x64 ..\qtbintreevis ```
    * ``` mingw32-make -j3 ```
    * ``` mingw32-make package ```
    * NOTE: One can run the bundled executable without installing from ```_CPack_Packages\win64\NSIS\QtBinTreeVis-0.1.1-win64```
